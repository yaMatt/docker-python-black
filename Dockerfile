FROM python:3.7-alpine3.10

LABEL maintainer="Matt Copperwaite matt@copperwaite.net"

ARG BLACK_VERSION=19.3b0
ARG BLACK_GROUP=blackgroup
ARG BLACK_USER=blackuser
ARG WORK_DIR=/src

RUN addgroup -S "$BLACK_GROUP" && adduser -S "$BLACK_USER" -G "$BLACK_GROUP"
RUN mkdir "$WORK_DIR" && chown "$BLACK_USER:$BLACK_GROUP" "$WORK_DIR"
USER "$BLACK_USER"
ENV PATH="${PATH}:/home/$BLACK_USER/.local/bin"
RUN pip3 install --user "black==$BLACK_VERSION"
WORKDIR ${WORK_DIR}

ENTRYPOINT ["black"]
