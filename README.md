# Docker Python Black

A docker image for the Python linting library [black](https://pypi.org/project/black/) because the most commonly used one by [Mozilla](https://hub.docker.com/r/mozilla/python-black) is currently [poorly maintained](https://web.archive.org/web/20190827192327/https://github.com/mozilla/docker-python-black/commits/master).

## Usage

```
docker run --rm --interactive --volume $(pwd)/example.py:/src/example.py yamatt/python-black:latest --target-version py36 .
```

## Features
- Fully linted
- Fully tested
- Lowest privileges
- Fixed versions
- Easy to contribute and maintain to because:
  - Use of [args](https://docs.docker.com/engine/reference/builder/#arg)
  - CI/CD
  - Friendly community

## Contribute

Hi, if you would like to contribute, and I would really appreciate it if you did, you can find and clone the repo on my [personal GitLab](https://git.copperwaite.net/matt/docker-python-black).

## License

### AGPLv3

    Copyright (C) 2019  Matt Copperwaite

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
